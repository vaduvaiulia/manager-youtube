'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('videos', {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true
      },
      videoId: {
        type: Sequelize.STRING,
        allowNull: false
      },
      videoTitle: {
        type: Sequelize.STRING,
        allowNull: false
      },
      videoThumbnail: {
        type: Sequelize.STRING,
        allowNull: false
      },
      videoEmbed: {
        type: Sequelize.STRING,
        allowNull: false
      },
      playlistId: {
        type: Sequelize.UUID,
        onDelete: 'CASCADE',
        references: {
          model: 'playlists',
          key: 'id'
        }
      },
      userId: {
        type: Sequelize.STRING,
        allowNull: false,
        onDelete: 'CASCADE',
        references: {
          model: 'users',
          key: 'id'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('videos');
  }
};
