const yup = require('yup');
const playlistService = require('../services/playlist');
const errorHandler = require('../utils/errorHandler');

const postSchema = yup.object().shape({
  name: yup.string().required()
});

const updateSchema = yup.object().shape({
  name: yup.string()
});

module.exports = {
  getPlaylists: async (req, res, next) => {
    try {
      const playlists = await playlistService.getPlaylists(req.user.id);
      res.status(200).send(playlists);
    } catch (e) {
      errorHandler(e, next);
    }
  },
  postPlaylist: async (req, res, next) => {
    try {
      await postSchema.validate(req.body);
      const playlist = await playlistService.createPlaylist({
        ...req.body,
        userId: req.user.id
      });
      res.status(201).send(playlist);
    } catch (e) {
      errorHandler(e, next);
    }
  },
  patchPlaylist: async (req, res, next) => {
    try {
      await updateSchema.validate(req.body);
      const playlist = await playlistService.updatePlaylist(
        req.params.id,
        req.body
      );
      res.status(200).send(playlist);
    } catch (e) {
      errorHandler(e, next);
    }
  },
  deletePlaylist: async (req, res, next) => {
    try {
      await playlistService.deletePlaylist(req.params.id);
      res.sendStatus(204);
    } catch (e) {
      errorHandler(e, next);
    }
  },
  getPlaylist: async (req, res, next) => {
    try {
      const playlist = await playlistService.getPlaylist(
        req.params.id,
        req.user.id
      );
      res.status(200).send(playlist);
    } catch (e) {
      errorHandler(e, next);
    }
  }
};
