const yup = require('yup');
const videoService = require('../services/video');
const youtubeService = require('../services/youtube');
const errorHandler = require('../utils/errorHandler');

const postSchema = yup.object().shape({
  videoId: yup.string().required(),
  videoTitle: yup.string().required(),
  videoThumbnail: yup.string().required()
});

const updateSchema = yup.object().shape({
  playlistId: yup.string()
});

module.exports = {
  getVideos: async (req, res, next) => {
    try {
      const videos = await videoService.getVideos(req.user.id);
      res.status(200).send(videos);
    } catch (e) {
      errorHandler(e, next);
    }
  },
  postVideo: async (req, res, next) => {
    try {
      await postSchema.validate(req.body);
      const youtubeResult = await youtubeService.getVideo(req.body.videoId);
      const video = await videoService.createVideo({
        ...req.body,
        videoEmbed: youtubeResult.data.items[0].player.embedHtml,
        userId: req.user.id
      });
      res.status(201).send(video);
    } catch (e) {
      errorHandler(e, next);
    }
  },
  patchVideo: async (req, res, next) => {
    try {
      await updateSchema.validate(req.body);
      const video = await videoService.updateVideo(req.params.id, req.body);
      res.status(200).send(video);
    } catch (e) {
      errorHandler(e, next);
    }
  },
  deleteVideo: async (req, res, next) => {
    try {
      await videoService.deleteVideo(req.params.id);
      res.sendStatus(204);
    } catch (e) {
      errorHandler(e, next);
    }
  },
  searchVideos: async (req, res, next) => {
    try {
      const youtubeResults = await youtubeService.searchVideos(req.query.query);
      const results = youtubeResults.data.items.map(item => ({
        videoId: item.id.videoId,
        videoTitle: item.snippet.title,
        videoThumbnail: item.snippet.thumbnails.default.url
      }));
      res.status(200).send(results);
    } catch (e) {
      errorHandler(e, next);
    }
  }
};
