'use strict';
module.exports = (sequelize, DataTypes) => {
  const video = sequelize.define(
    'video',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true
      },
      videoId: {
        type: DataTypes.STRING,
        allowNull: false
      },
      videoTitle: {
        type: DataTypes.STRING,
        allowNull: false
      },
      videoThumbnail: {
        type: DataTypes.STRING,
        allowNull: false
      },
      videoEmbed: {
        type: DataTypes.STRING,
        allowNull: false
      },
      playlistId: DataTypes.UUID,
      userId: {
        type: DataTypes.STRING,
        allowNull: false
      }
    },
    {}
  );
  video.associate = function(models) {
    video.belongsTo(models.user, { onDelete: 'CASCADE' });
    video.belongsTo(models.playlist);
  };
  return video;
};
