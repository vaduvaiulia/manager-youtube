'use strict';
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

module.exports = (sequelize, DataTypes) => {
  const user = sequelize.define(
    'user',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false
      }
    },
    {}
  );
  user.associate = function(models) {
    user.hasMany(models.video);
    user.hasMany(models.playlist);
  };

  user.beforeCreate(async user => {
    const hash = await bcrypt.hash(user.password, 10);
    user.password = hash;
  });

  user.prototype.isPasswordValid = function(password) {
    return bcrypt.compareSync(password, this.password);
  };

  user.prototype.authJSON = function() {
    const today = new Date();
    const exp = new Date(today);

    exp.setDate(today.getDate() + 60);
    const token = jwt.sign(
      {
        context: {
          email: this.email
        },
        exp: parseInt(exp.getTime() / 1000)
      },
      process.env.JWT_SECRET
    );

    return {
      token
    };
  };

  return user;
};
