'use strict';
module.exports = (sequelize, DataTypes) => {
  const playlist = sequelize.define(
    'playlist',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false
      },
      userId: {
        type: DataTypes.STRING,
        allowNull: false
      }
    },
    {}
  );
  playlist.associate = function(models) {
    playlist.belongsTo(models.user, { onDelete: 'CASCADE' });
    playlist.hasMany(models.video);
  };
  return playlist;
};
