const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();

require('./models');
const passport = require('./config/passport');

app.use(passport.initialize());

app.use(require('method-override')());
app.use(cors());

app.use(require('morgan')('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

if (process.env.NODE_ENV === 'DEV') {
  // eslint-disable-next-line
  app.use((err, req, res, next) => {
    res.status(err.status || 500).send('error', {
      message: err.message,
      error: err
    });
  });
}

// eslint-disable-next-line
app.use(function(err, req, res, next) {
  console.log(err);
  res.status(err.status || 500).send('error', {
    message: err.message,
    error: {}
  });
});

app.use(require('./routes/auth'));
app.use(require('./routes/video'));
app.use(require('./routes/playlist'));

const server = app.listen(process.env.PORT || 3000, () => {
  console.log('Listening on port ' + server.address().port);
});

module.exports = app;
