const passport = require('../config/passport');
const router = require('express').Router();
const playlistController = require('../controllers/playlist');

router.post(
  '/playlists',
  passport.authenticate('jwt'),
  playlistController.postPlaylist
);
router.get(
  '/playlists',
  passport.authenticate('jwt'),
  playlistController.getPlaylists
);
router.get(
  '/playlists/:id',
  passport.authenticate('jwt'),
  playlistController.getPlaylist
);
router.patch(
  '/playlists/:id',
  passport.authenticate('jwt'),
  playlistController.patchPlaylist
);
router.delete(
  '/playlists/:id',
  passport.authenticate('jwt'),
  playlistController.deletePlaylist
);

module.exports = router;
