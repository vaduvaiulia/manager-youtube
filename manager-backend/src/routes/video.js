const passport = require('../config/passport');
const router = require('express').Router();
const videoController = require('../controllers/video');

router.post('/videos', passport.authenticate('jwt'), videoController.postVideo);
router.get('/videos', passport.authenticate('jwt'), videoController.getVideos);
router.patch(
  '/videos/:id',
  passport.authenticate('jwt'),
  videoController.patchVideo
);
router.delete(
  '/videos/:id',
  passport.authenticate('jwt'),
  videoController.deleteVideo
);
router.get(
  '/search',
  passport.authenticate('jwt'),
  videoController.searchVideos
);

module.exports = router;
