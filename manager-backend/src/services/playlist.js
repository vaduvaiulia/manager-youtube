const { playlist, video } = require('../models');

module.exports = {
  createPlaylist: async data => {
    try {
      return await playlist.create({
        ...data
      });
    } catch (e) {
      throw e;
    }
  },
  findPlaylist: async data => {
    try {
      const result = await playlist.findOne({ where: { ...data } });
      if (result) return result;
      else {
        let err = new Error('Not Found');
        err.status = 404;
        throw err;
      }
    } catch (e) {
      throw e;
    }
  },
  updatePlaylist: async (id, data) => {
    try {
      await playlist.update(
        {
          ...data
        },
        {
          where: {
            id
          }
        }
      );
      const result = await playlist.findByPk(id);
      if (result) return result;
      else {
        let err = new Error('Not Found');
        err.status = 404;
        throw err;
      }
    } catch (e) {
      throw e;
    }
  },
  deletePlaylist: async id => {
    try {
      const result = await playlist.destroy({
        where: {
          id: id
        }
      });
      if (result) return result;
      else {
        let err = new Error('Not Found');
        err.status = 404;
        throw err;
      }
    } catch (e) {
      throw e;
    }
  },
  getPlaylists: async id => {
    try {
      return await playlist.findAll({
        where: {
          userId: id
        }
      });
    } catch (e) {
      throw e;
    }
  },
  getPlaylist: async (id, userId) => {
    try {
      const result = await playlist.findByPk(id, {
        where: {
          userId: userId
        },
        include: [
          {
            model: video
          }
        ]
      });
      if (result) return result;
      else {
        let err = new Error('Not Found');
        err.status = 404;
        throw err;
      }
    } catch (e) {
      throw e;
    }
  }
};
