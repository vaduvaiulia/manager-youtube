const { video } = require('../models');

module.exports = {
  createVideo: async data => {
    try {
      return await video.create({
        ...data
      });
    } catch (e) {
      throw e;
    }
  },
  findVideo: async data => {
    try {
      const result = await video.findOne({ where: { ...data } });
      if (result) return result;
      else {
        let err = new Error('Not Found');
        err.status = 404;
        throw err;
      }
    } catch (e) {
      throw e;
    }
  },
  updateVideo: async (id, data) => {
    try {
      await video.update(
        {
          ...data
        },
        {
          where: {
            id
          }
        }
      );
      const result = await video.findByPk(id);
      if (result) return result;
      else {
        let err = new Error('Not Found');
        err.status = 404;
        throw err;
      }
    } catch (e) {
      throw e;
    }
  },
  deleteVideo: async id => {
    try {
      const result = await video.destroy({
        where: {
          id: id
        }
      });
      if (result) return result;
      else {
        let err = new Error('Not Found');
        err.status = 404;
        throw err;
      }
    } catch (e) {
      throw e;
    }
  },
  getVideos: async id => {
    try {
      return await video.findAll({
        where: {
          userId: id
        }
      });
    } catch (e) {
      throw e;
    }
  }
};
