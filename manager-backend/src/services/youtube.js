const youtube = require('../config/youtube');

module.exports = {
  searchVideos: async query => {
    try {
      return await youtube.search.list({
        part: 'id, snippet',
        q: query,
        type: 'video',
        maxResults: 25
      });
    } catch (e) {
      throw e;
    }
  },
  getVideo: async id => {
    try {
      return await youtube.videos.list({
        id,
        part: 'player'
      });
    } catch (e) {
      throw e;
    }
  }
};
