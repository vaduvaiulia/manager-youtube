import loadingReducer from "./loading";
import errorReducer from "./error";
import authReducer from "./auth";
import videoReducer from "./video";
import playlistReducer from "./playlist";

export const loading = loadingReducer;
export const error = errorReducer;
export const auth = authReducer;
export const video = videoReducer;
export const playlist = playlistReducer;
