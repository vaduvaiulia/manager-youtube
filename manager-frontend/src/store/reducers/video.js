import { createReducer } from "redux-act";

import { videoActions } from "../actions";

const initialState = {
  videos: [],
  searchResults: []
};

export default createReducer(
  {
    [videoActions.searchVideosSuccess]: (state, payload) => {
      if (payload) {
        const searchedVideos = payload.map(item => ({
          ...item,
          favorite: state.videos.length
            ? state.videos.some(video => video.videoId === item.videoId)
            : false
        }));
        return {
          ...state,
          searchResults: [...searchedVideos]
        };
      }
      return state;
    },
    [videoActions.getVideosSuccess]: (state, payload) => ({
      ...state,
      videos: payload ? [...payload] : []
    }),
    [videoActions.postVideoSuccess]: (state, payload) => {
      const stateCopy = { ...state };
      const index = stateCopy.searchResults.findIndex(
        item => item.videoId === payload.videoId
      );
      stateCopy.videos.push(payload);
      stateCopy.searchResults[index].favorite = true;
      return { ...stateCopy };
    },
    [videoActions.deleteVideoSuccess]: (state, payload) => {
      const stateCopy = { ...state };
      const index = state.videos.findIndex(item => item.id === payload.id);
      const searchResultIndex = stateCopy.searchResults.findIndex(
        item => item.videoId === payload.videoId
      );
      if (searchResultIndex)
        stateCopy.searchResults[searchResultIndex].favorite = false;
      stateCopy.videos.splice(index, 1);
      return { ...stateCopy };
    },
    [videoActions.updateVideoSuccess]: (state, payload) => {
      const index = state.videos.findIndex(item => item.id === payload.id);
      return {
        ...state,
        videos: state.videos.map((item, ind) => {
          if (index !== ind) return item;
          return {
            ...payload
          };
        })
      };
    }
  },
  initialState
);
