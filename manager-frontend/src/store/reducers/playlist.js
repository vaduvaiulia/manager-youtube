import { createReducer } from "redux-act";

import { playlistActions } from "../actions";

const initialState = {
  playlists: [],
  currentPlaylist: null
};

export default createReducer(
  {
    [playlistActions.getPlaylistsSuccess]: (state, payload) => ({
      ...state,
      playlists: payload ? [...payload] : []
    }),
    [playlistActions.postPlaylistSuccess]: (state, payload) => {
      state.playlists.push(payload);
      return state;
    },
    [playlistActions.getPlaylistSuccess]: (state, payload) => ({
      ...state,
      currentPlaylist: { ...payload }
    })
  },
  initialState
);
