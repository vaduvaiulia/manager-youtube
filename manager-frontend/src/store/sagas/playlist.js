import { put, call } from "redux-saga/effects";
import { playlistActions } from "../actions";

export function* getPlaylists(api) {
  yield put(playlistActions.getPlaylistsRequest());
  try {
    const response = yield call(api.getPlaylists);
    yield put(playlistActions.getPlaylistsSuccess(response.data));
  } catch (e) {
    yield put(playlistActions.getPlaylistsFailure(e));
  }
}

export function* postPlaylist(api, object) {
  yield put(playlistActions.postPlaylistRequest());
  try {
    const response = yield call(api.postPlaylist, object.payload);
    yield put(playlistActions.postPlaylistSuccess(response.data));
  } catch (e) {
    yield put(playlistActions.postPlaylistFailure(e));
  }
}

export function* getPlaylist(api, object) {
  yield put(playlistActions.getPlaylistRequest());
  try {
    const response = yield call(api.getPlaylist, object.payload);
    yield put(playlistActions.getPlaylistSuccess(response.data));
  } catch (e) {
    yield put(playlistActions.getPlaylistFailure(e));
  }
}
