import createSagaMiddleware from "redux-saga";
import { takeLatest, all } from "redux-saga/effects";
import api from "../../lib/api";

import * as auth from "./auth";
import * as video from "./video";
import * as playlist from "./playlist";
import { authTypes, videoTypes, playlistTypes } from "../actionTypes";

const API = api();

function* watchAuth() {
  yield all([
    takeLatest(authTypes.LOGIN, auth.login, API),
    takeLatest(authTypes.REGISTER, auth.register, API),
    takeLatest(authTypes.CHECK_AUTH, auth.checkAuth),
    takeLatest(authTypes.LOGOUT, auth.logout)
  ]);
}

function* watchVideos() {
  yield all([
    takeLatest(videoTypes.SEARCH_VIDEOS, video.searchVideos, API),
    takeLatest(videoTypes.GET_VIDEOS, video.getVideos, API),
    takeLatest(videoTypes.POST_VIDEO, video.postVideo, API),
    takeLatest(videoTypes.DELETE_VIDEO, video.deleteVideo, API),
    takeLatest(videoTypes.UPDATE_VIDEO, video.updateVideo, API)
  ]);
}

function* watchPlaylists() {
  yield all([
    takeLatest(playlistTypes.GET_PLAYLISTS, playlist.getPlaylists, API),
    takeLatest(playlistTypes.GET_PLAYLIST, playlist.getPlaylist, API),
    takeLatest(playlistTypes.POST_PLAYLIST, playlist.postPlaylist, API)
  ]);
}

export const sagaMiddleware = createSagaMiddleware();
export const authSaga = watchAuth;
export const videoSaga = watchVideos;
export const playlistSaga = watchPlaylists;
