import { put, call } from "redux-saga/effects";
import { videoActions } from "../actions";

export function* searchVideos(api, object) {
  yield put(videoActions.searchVideosRequest());
  try {
    const response = yield call(api.getSearch, object.payload);
    yield put(videoActions.searchVideosSuccess(response.data));
  } catch (e) {
    yield put(videoActions.searchVideosFailure(e));
  }
}

export function* getVideos(api) {
  yield put(videoActions.getVideosRequest());
  try {
    const response = yield call(api.getVideos);
    yield put(videoActions.getVideosSuccess(response.data));
  } catch (e) {
    yield put(videoActions.getVideosFailure(e));
  }
}

export function* postVideo(api, object) {
  yield put(videoActions.postVideoRequest());
  try {
    const response = yield call(api.postVideo, object.payload);
    yield put(videoActions.postVideoSuccess(response.data));
  } catch (e) {
    yield put(videoActions.postVideoFailure(e));
  }
}

export function* deleteVideo(api, object) {
  yield put(videoActions.deleteVideoRequest());
  try {
    yield call(api.deleteVideo, object.payload.id);
    yield put(
      videoActions.deleteVideoSuccess({
        id: object.payload.id,
        videoId: object.payload.videoId
      })
    );
  } catch (e) {
    yield put(videoActions.deleteVideoFailure(e));
  }
}

export function* updateVideo(api, object) {
  yield put(videoActions.updateVideoRequest());
  try {
    const response = yield call(
      api.updateVideo,
      object.payload.id,
      object.payload.video
    );
    yield put(videoActions.updateVideoSuccess(response.data));
  } catch (e) {
    yield put(videoActions.updateVideoFailure(e));
  }
}
