import auth from "./auth";
import video from "./video";
import playlist from "./playlist";

export const authTypes = auth;
export const videoTypes = video;
export const playlistTypes = playlist;
