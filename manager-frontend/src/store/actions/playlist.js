import { createAction } from "redux-act";

import { playlistTypes } from "../actionTypes";

const getPlaylists = createAction(playlistTypes.GET_PLAYLISTS);
const getPlaylistsRequest = createAction(playlistTypes.GET_PLAYLISTS_REQUEST);
const getPlaylistsSuccess = createAction(playlistTypes.GET_PLAYLISTS_SUCCESS);
const getPlaylistsFailure = createAction(playlistTypes.GET_PLAYLISTS_FAILURE);

const getPlaylist = createAction(playlistTypes.GET_PLAYLIST);
const getPlaylistRequest = createAction(playlistTypes.GET_PLAYLIST_REQUEST);
const getPlaylistSuccess = createAction(playlistTypes.GET_PLAYLIST_SUCCESS);
const getPlaylistFailure = createAction(playlistTypes.GET_PLAYLIST_FAILURE);

const postPlaylist = createAction(playlistTypes.POST_PLAYLIST);
const postPlaylistRequest = createAction(playlistTypes.POST_PLAYLIST_REQUEST);
const postPlaylistSuccess = createAction(playlistTypes.POST_PLAYLIST_SUCCESS);
const postPlaylistFailure = createAction(playlistTypes.POST_PLAYLIST_FAILURE);

export default {
  getPlaylist,
  getPlaylistRequest,
  getPlaylistSuccess,
  getPlaylistFailure,
  getPlaylists,
  getPlaylistsRequest,
  getPlaylistsSuccess,
  getPlaylistsFailure,
  postPlaylist,
  postPlaylistRequest,
  postPlaylistSuccess,
  postPlaylistFailure
}
