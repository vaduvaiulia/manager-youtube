import { createAction } from "redux-act";

import { authTypes } from "../actionTypes";

const login = createAction(authTypes.LOGIN);
const loginRequest = createAction(authTypes.LOGIN_REQUEST);
const loginSuccess = createAction(authTypes.LOGIN_SUCCESS);
const loginFailure = createAction(authTypes.LOGIN_FAILURE);

const register = createAction(authTypes.REGISTER);
const registerRequest = createAction(authTypes.REGISTER_REQUEST);
const registerSuccess = createAction(authTypes.REGISTER_SUCCESS);
const registerFailure = createAction(authTypes.REGISTER_FAILURE);

const logout = createAction(authTypes.LOGOUT);
const logoutSuccess = createAction(authTypes.LOGOUT_SUCCESS);

const checkAuth = createAction(authTypes.CHECK_AUTH);

export default {
  login,
  loginRequest,
  loginSuccess,
  loginFailure,
  register,
  registerRequest,
  registerFailure,
  registerSuccess,
  logout,
  logoutSuccess,
  checkAuth
}
