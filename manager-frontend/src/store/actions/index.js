import auth from "./auth";
import video from "./video";
import playlist from "./playlist";

export const authActions = auth;
export const videoActions = video;
export const playlistActions = playlist;
