import { createAction } from "redux-act";

import { videoTypes } from "../actionTypes";

const getVideos = createAction(videoTypes.GET_VIDEOS);
const getVideosRequest = createAction(videoTypes.GET_VIDEOS_REQUEST);
const getVideosSuccess = createAction(videoTypes.GET_VIDEOS_SUCCESS);
const getVideosFailure = createAction(videoTypes.GET_VIDEOS_FAILURE);

const postVideo = createAction(videoTypes.POST_VIDEO);
const postVideoRequest = createAction(videoTypes.POST_VIDEO_REQUEST);
const postVideoSuccess = createAction(videoTypes.POST_VIDEO_SUCCESS);
const postVideoFailure = createAction(videoTypes.POST_VIDEO_FAILURE);

const deleteVideo = createAction(videoTypes.DELETE_VIDEO);
const deleteVideoRequest = createAction(videoTypes.DELETE_VIDEO_REQUEST);
const deleteVideoSuccess = createAction(videoTypes.DELETE_VIDEO_SUCCESS);
const deleteVideoFailure = createAction(videoTypes.DELETE_VIDEO_FAILURE);

const updateVideo = createAction(videoTypes.UPDATE_VIDEO);
const updateVideoRequest = createAction(videoTypes.UPDATE_VIDEO_REQUEST);
const updateVideoSuccess = createAction(videoTypes.UPDATE_VIDEO_SUCCESS);
const updateVideoFailure = createAction(videoTypes.UPDATE_VIDEO_FAILURE);

const searchVideos = createAction(videoTypes.SEARCH_VIDEOS);
const searchVideosRequest = createAction(videoTypes.SEARCH_VIDEOS_REQUEST);
const searchVideosSuccess = createAction(videoTypes.SEARCH_VIDEOS_SUCCESS);
const searchVideosFailure = createAction(videoTypes.SEARCH_VIDEOS_FAILURE);

export default {
  getVideos,
  getVideosRequest,
  getVideosSuccess,
  getVideosFailure,
  postVideo,
  postVideoRequest,
  postVideoSuccess,
  postVideoFailure,
  deleteVideo,
  deleteVideoRequest,
  deleteVideoSuccess,
  deleteVideoFailure,
  updateVideo,
  updateVideoRequest,
  updateVideoSuccess,
  updateVideoFailure,
  searchVideos,
  searchVideosRequest,
  searchVideosSuccess,
  searchVideosFailure
};
