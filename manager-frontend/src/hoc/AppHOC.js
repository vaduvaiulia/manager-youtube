import React, { Component } from "react";

import Header from "../containers/Header";

const appHOC = WrappedComponent =>
  class extends Component {
    render() {
      return (
        <React.Fragment>
          <Header />
          <WrappedComponent {...this.props} />
        </React.Fragment>
      );
    }
  };

export default appHOC;
