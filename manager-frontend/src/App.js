import React, { Component } from "react";
import { Router, Route, Switch } from "react-router-dom";

import { connect } from "react-redux";
import authActions from "./store/actions/auth";

import history from "./lib/history";

import PrivateRoute from "./containers/PrivateRoute";

import Login from "./views/Login";
import Register from "./views/Register";
import SearchVideos from "./views/SearchVideos";
import VideoList from "./views/VideoList";
import PlaylistList from "./views/PlaylistList";
import PlaylistDetail from "./views/PlaylistDetail";

class App extends Component {
  componentDidMount() {
    this.props.checkAuth();
  }

  render() {
    return (
      <Router history={history}>
        <div className="App">
          <Switch>
            <Route path="/login" component={Login} />
            <Route path="/register" component={Register} />
            <PrivateRoute exact component={SearchVideos} path="/" />
            <PrivateRoute exact component={VideoList} path="/videos" />
            <PrivateRoute exact component={PlaylistList} path="/playlists" />
            <PrivateRoute
              exact
              component={PlaylistDetail}
              path="/playlists/:id"
            />
          </Switch>
        </div>
      </Router>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  checkAuth: () => dispatch(authActions.checkAuth())
});

export default connect(
  null,
  mapDispatchToProps
)(App);
