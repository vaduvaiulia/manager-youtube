import axios from "axios";
import * as auth from "./auth";

const create = (baseURL = process.env.REACT_APP_API_URL) => {
  const api = axios.create({
    baseURL,
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json"
    }
  });

  api.interceptors.request.use(
    config => {
      const token = auth.getToken();
      if (token) {
        config.headers.Authorization = `JWT ${token}`;
      }
      return config;
    },
    err => Promise.reject(err)
  );

  const postLogin = loginObj => api.post("/login", loginObj);
  const postRegister = registerObj => api.post("/register", registerObj);
  const getSearch = query => api.get(`/search?query=${query}`);
  const getVideos = () => api.get("/videos");
  const postVideo = videoObj => api.post("/videos", videoObj);
  const deleteVideo = id => api.delete(`/videos/${id}`);
  const updateVideo = (id, videoObj) => api.patch(`/videos/${id}`, videoObj);
  const getPlaylists = () => api.get("/playlists");
  const getPlaylist = id => api.get(`/playlists/${id}`);
  const postPlaylist = playlistObj => api.post("/playlists", playlistObj);

  return {
    postLogin,
    postRegister,
    getSearch,
    getVideos,
    postVideo,
    deleteVideo,
    updateVideo,
    getPlaylist,
    getPlaylists,
    postPlaylist
  };
};

export default create;
