export const getToken = () => {
  return localStorage.getItem("manager-app");
};

export const deleteToken = () => {
  return localStorage.removeItem("manager-app");
};

export const setToken = token => {
  return localStorage.setItem("manager-app", token);
};
