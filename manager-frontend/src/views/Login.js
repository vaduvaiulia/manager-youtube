import React, { Component } from "react";
import { Link } from "react-router-dom";
import { withFormik } from "formik";
import * as yup from "yup";

import { connect } from "react-redux";
import authActions from "../store/actions/auth";
import { createLoadingSelector } from "../store/selectors";

import Input from "../components/Input";
import Button from "../components/Button";

const enhancer = withFormik({
  mapPropsToValues: () => ({
    email: "",
    password: ""
  }),
  validationSchema: yup.object().shape({
    email: yup
      .string()
      .email()
      .required(),
    password: yup.string().required()
  }),
  handleSubmit: (values, props) => {
    props.props.login({
      email: values.email,
      password: values.password
    });
  }
});

class Login extends Component {
  render() {
    return (
      <div className="container">
        <div
          style={{ height: "100vh" }}
          className="row justify-content-center align-items-center"
        >
          <div className="col-sm-6">
            <div className="card">
              <div className="card-header">
                <h3 className="card-title">Login</h3>
              </div>
              <div className="card-body">
                <form>
                  <Input
                    error={this.props.errors.email}
                    value={this.props.values.email}
                    onChange={this.props.handleChange("email")}
                    placeholder="E-mail"
                    label="E-mail"
                    type="email"
                  />
                  <Input
                    error={this.props.errors.password}
                    value={this.props.values.password}
                    onChange={this.props.handleChange("password")}
                    label="Password"
                    placeholder="Password"
                    type="password"
                  />
                </form>
              </div>
              <div className="card-footer">
                <Button
                  text="Login"
                  onClick={this.props.handleSubmit}
                  type="primary"
                />
                <Link className="btn btn-link" to="/register">
                  Register
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const loading = createLoadingSelector(["LOGIN"]);

const mapStateToProps = state => ({
  isFetching: loading(state)
});

const mapDispatchToProps = dispatch => ({
  login: loginObj => dispatch(authActions.login(loginObj))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(enhancer(Login));
