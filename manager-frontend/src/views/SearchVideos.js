import React, { Component } from "react";

import { withFormik } from "formik";
import * as yup from "yup";

import { connect } from "react-redux";
import { videoActions } from "../store/actions";

import AppHOC from "../hoc/AppHOC";

import Input from "../components/Input";
import Button from "../components/Button";
import VideoListItem from "../components/VideoListItem";

const enhancer = withFormik({
  mapPropsToValues: () => ({
    query: ""
  }),
  validationSchema: yup.object().shape({
    query: yup.string().required()
  }),
  handleSubmit: (values, { props, resetForm }) => {
    props.searchVideos(values.query);
    resetForm();
  }
});

class SearchVideos extends Component {
  componentDidMount() {
    this.props.getVideos();
  }
  render() {
    const actionBar = item => (
      <React.Fragment>
        <Button
          type="success"
          disabled={item.favorite}
          text="Favorite"
          onClick={() => this.props.postVideo(item)}
        />
      </React.Fragment>
    );

    const videos = this.props.searchResults.length ? (
      this.props.searchResults.map(item => (
        <VideoListItem
          key={item.videoId}
          title={item.videoTitle}
          thumbnail={item.videoThumbnail}
          actionBar={actionBar(item)}
        />
      ))
    ) : (
      <p>No videos to show.</p>
    );
    return (
      <div className="container mt-2">
        <div className="row justify-content-center">
          <div className="col-sm-6">
            <Input
              error={this.props.errors.query}
              value={this.props.values.query}
              onChange={this.props.handleChange("query")}
              placeholder="Search"
              type="text"
            />
          </div>
          <div className="col-sm-2">
            <Button
              text="Search"
              m
              disabled={this.props.isSubmitting}
              onClick={this.props.handleSubmit}
              type="secondary"
            />
          </div>
        </div>
        <div className="row justify-content-center">
          <div className="col-sm-8">
            <ul className="list-unstyled">{videos}</ul>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  searchResults: state.video.searchResults,
  videos: state.video.videos
});

const mapDispatchToProps = dispatch => ({
  searchVideos: query => dispatch(videoActions.searchVideos(query)),
  getVideos: () => dispatch(videoActions.getVideos()),
  postVideo: item => dispatch(videoActions.postVideo(item))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppHOC(enhancer(SearchVideos)));
