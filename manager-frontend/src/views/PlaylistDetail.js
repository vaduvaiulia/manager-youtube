import React, { Component } from "react";

import { connect } from "react-redux";
import { videoActions, playlistActions } from "../store/actions";

import AppHOC from "../hoc/AppHOC";

import Button from "../components/Button";
import VideoListItem from "../components/VideoListItem";

class PlaylistDetail extends Component {
  componentDidMount() {
    this.props.getPlaylist(this.props.match.params.id);
  }
  render() {
    const actionBar = item => (
      <React.Fragment>
        <Button
          type="danger"
          text="Delete"
          onClick={() =>
            this.props.deleteVideo({ id: item.id, videoId: item.videoId })
          }
        />
      </React.Fragment>
    );

    const videos =
      this.props.currentPlaylist && this.props.currentPlaylist.videos.length ? (
        this.props.currentPlaylist.videos.map(item => (
          <VideoListItem
            key={item.videoId}
            title={item.videoTitle}
            thumbnail={item.videoThumbnail}
            actionBar={actionBar(item)}
          />
        ))
      ) : (
        <p>No videos to show.</p>
      );
    return (
      <div className="container mt-2">
        <div className="row justify-content-center">
          <div className="col-sm-8">
            <ul className="list-unstyled">{videos}</ul>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  currentPlaylist: state.playlist.currentPlaylist
});

const mapDispatchToProps = dispatch => ({
  getPlaylist: id => dispatch(playlistActions.getPlaylist(id)),
  deleteVideo: id => dispatch(videoActions.deleteVideo(id))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppHOC(PlaylistDetail));
