import React, { Component } from "react";

import { connect } from "react-redux";
import { videoActions, playlistActions } from "../store/actions";

import AppHOC from "../hoc/AppHOC";

import Button from "../components/Button";
import VideoListItem from "../components/VideoListItem";

class VideoList extends Component {
  componentDidMount() {
    this.props.getVideos();
    this.props.getPlaylists();
  }
  render() {
    const actionBar = item => (
      <React.Fragment>
        <select
          className="custom-select"
          value={item.playlistId ? item.playlistId : ""}
          onChange={e =>
            this.props.updateVideo({
              id: item.id,
              video: {
                playlistId: e.target.value
              }
            })
          }
        >
          <option value={""}>None</option>
          {this.props.playlists.map(playlist => (
            <option key={playlist.id} value={playlist.id}>
              {playlist.name}
            </option>
          ))}
        </select>
        <Button
          type="danger"
          text="Delete"
          onClick={() =>
            this.props.deleteVideo({ id: item.id, videoId: item.videoId })
          }
        />
      </React.Fragment>
    );

    const videos = this.props.videos.length ? (
      this.props.videos.map(item => (
        <VideoListItem
          key={item.videoId}
          title={item.videoTitle}
          thumbnail={item.videoThumbnail}
          actionBar={actionBar(item)}
        />
      ))
    ) : (
      <p>No videos to show.</p>
    );
    return (
      <div className="container mt-2">
        <div className="row justify-content-center">
          <div className="col-sm-8">
            <ul className="list-unstyled">{videos}</ul>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  videos: state.video.videos,
  playlists: state.playlist.playlists
});

const mapDispatchToProps = dispatch => ({
  getPlaylists: () => dispatch(playlistActions.getPlaylists()),
  getVideos: () => dispatch(videoActions.getVideos()),
  updateVideo: item => dispatch(videoActions.updateVideo(item)),
  deleteVideo: id => dispatch(videoActions.deleteVideo(id))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppHOC(VideoList));
