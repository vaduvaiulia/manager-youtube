import React, { Component } from "react";

import { Link } from "react-router-dom";

import { connect } from "react-redux";
import { playlistActions } from "../store/actions";

import AppHOC from "../hoc/AppHOC";
import AddPlaylistForm from "../containers/AddPlaylistForm";
import Button from "../components/Button";

class PlaylistList extends Component {
  state = {
    playlistFormVisible: false
  };

  componentDidMount() {
    this.props.getPlaylists();
  }

  closeForm = () => {
    this.setState({
      playlistFormVisible: false
    });
  };

  openForm = () => {
    this.setState({
      playlistFormVisible: true
    });
  };

  render() {
    const playlists = this.props.playlists.length ? (
      this.props.playlists.map(item => (
        <Link
          className="list-group-item"
          key={item.id}
          title={item.videoTitle}
          to={`/playlists/${item.id}`}
        >
          {item.name}
        </Link>
      ))
    ) : (
      <p>No playlists to show.</p>
    );
    return (
      <div className="container mt-2">
        <div className="row">
          <Button text="Add playlist" type="primary" onClick={this.openForm} />
        </div>
        <div className="row justify-content-center">
          <div className="col-sm-8">
            <ul className="list-group">{playlists}</ul>
          </div>
        </div>
        <AddPlaylistForm
          visible={this.state.playlistFormVisible}
          closeModal={this.closeForm}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  playlists: state.playlist.playlists
});

const mapDispatchToProps = dispatch => ({
  getPlaylists: () => dispatch(playlistActions.getPlaylists()),
  postPlaylist: item => dispatch(playlistActions.postPlaylist(item))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppHOC(PlaylistList));
