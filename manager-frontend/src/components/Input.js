import React from "react";

const input = props => {
  const error = props.error ? (
    <p style={{ display: "block" }} className="invalid-feedback">
      {props.error}
    </p>
  ) : null;
  const label = props.label ? <label>{props.label}</label> : null;
  return (
    <div className="form-group">
      {label}
      <input
        className="form-control"
        type={props.type}
        placeholder={props.placeholder}
        value={props.value}
        onChange={props.onChange}
      />
      {error}
    </div>
  );
};

export default input;
