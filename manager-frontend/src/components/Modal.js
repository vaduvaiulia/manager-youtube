import React from "react";

const modal = props => {
  const header = props.title ? (
    <div className="modal-header">
      <h5 className="modal-title">{props.title}</h5>
    </div>
  ) : null;
  const body = props.body ? (
    <div className="modal-body">{props.body}</div>
  ) : null;
  const footer = props.footer ? (
    <div className="modal-footer">{props.footer}</div>
  ) : null;
  const backdrop = props.visible ? (
    <div className="modal-backdrop fade show" />
  ) : null;
  return props.visible ? (
    <React.Fragment>
      <div
        style={{ display: props.visible ? "block" : "none" }}
        className={`modal fade ${props.visible ? "show" : ""}`}
      >
        <div className="modal-dialog">
          <div className="modal-content">
            {header}
            {body}
            {footer}
          </div>
        </div>
      </div>
      {backdrop}
    </React.Fragment>
  ) : null;
};

export default modal;
