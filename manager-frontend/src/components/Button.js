import React from "react";

const button = props => (
  <button
    type="button"
    onClick={props.onClick}
    disabled={props.disabled}
    className={`btn btn-${props.type}`}
  >
    {props.text}
  </button>
);

export default button;
