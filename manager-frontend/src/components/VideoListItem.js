import React from "react";

const videoListItem = props => (
  <div className="media">
    <img className="mr-3" src={props.thumbnail} alt={props.title} />
    <div className="media-body">
      <h5 className="mt-0">{props.title}</h5>
      {props.actionBar}
    </div>
  </div>
);

export default videoListItem;
