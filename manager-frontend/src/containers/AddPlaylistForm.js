import React, { Component } from "react";

import * as yup from "yup";
import { withFormik } from "formik";

import { connect } from "react-redux";
import { playlistActions } from "../store/actions";

import Modal from "../components/Modal";
import Input from "../components/Input";
import Button from "../components/Button";

const enhancer = withFormik({
  mapPropsToValues: () => ({
    name: ""
  }),
  validationSchema: yup.object().shape({
    name: yup.string().required()
  }),
  handleSubmit: (values, { props, resetForm }) => {
    props.postPlaylist({ name: values.name });
    resetForm();
    props.closeModal();
  }
});

class AddPlaylistForm extends Component {
  render() {
    return (
      <Modal
        visible={this.props.visible}
        title="Add playlist"
        body={
          <Input
            error={this.props.errors.name}
            value={this.props.values.name}
            onChange={this.props.handleChange("name")}
            placeholder="Name"
            label="Name"
            type="name"
          />
        }
        footer={
          <React.Fragment>
            <Button
              text="Add playlist"
              onClick={this.props.handleSubmit}
              type="primary"
            />
            <Button
              text="Close"
              onClick={this.props.closeModal}
              type="danger"
            />
          </React.Fragment>
        }
      />
    );
  }
}

const mapDispatchToProps = dispatch => ({
  postPlaylist: playlistObj =>
    dispatch(playlistActions.postPlaylist(playlistObj))
});

export default connect(
  null,
  mapDispatchToProps
)(enhancer(AddPlaylistForm));
