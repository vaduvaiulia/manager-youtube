import React, { Component } from "react";
import { Link } from "react-router-dom";

import { connect } from "react-redux";
import { authActions } from "../store/actions";

import Button from "../components/Button";

class Header extends Component {
  render() {
    return (
      <nav className="navbar navbar-expand navbar-dark bg-primary justify-content-between">
        <div className="d-flex">
          <Link className="navbar-brand" to="/">
            Youtube Manager
          </Link>
          <div className="navbar-nav">
            <Link className="nav-item nav-link" to="/">
              Home
            </Link>
            <Link className="nav-item nav-link" to="/playlists">
              My playlists
            </Link>
            <Link className="nav-item nav-link" to="/videos">
              My videos
            </Link>
          </div>
        </div>
        <Button
          className="float-right"
          text="Log out"
          type="secondary"
          onClick={() => this.props.logout()}
        />
      </nav>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(authActions.logout())
});

export default connect(
  null,
  mapDispatchToProps
)(Header);
